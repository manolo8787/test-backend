<?php

/**
 * @author Guilherme Mendes <guilherme8787@gmail.com>
 * 
 * Essa classe foi escrita em PHP puro e serve para gerenciar uma conexão ao banco de dados usadno mysqli orientado a objetos
 * 
 */
class Conn
{
    private $address = "localhost";
    private $user = "root";
    private $pass = "";
    private $banco = "";

    /**
    * @description retorna instancia de conexão
    *
    * @return object
    **/
    public function link()
    {
        $link = new mysqli($this->address, $this->user, $this->pass, $this->banco);
        if ($link->connect_errno) {
            echo $link->connect_error . ' N deu';
            return 0;
        } else {
            return $link;
        }
    }

    /**
    * @description muda o banco, antes ou durante a conexão
    *
    * @param string $banco nome do banco a ser selecionado
    * @param object $link instancia de conexão, pode ser NULL
    * @return object
    **/
    private function setBanco($banco, $link){
        if($link != null){
            @$link->select_db($banco);
        }
        $this->banco = $banco;
        
    }

    /**
    * @description Executa uma query e retorna o resutado 
    *
    * @param object $link instancia de conexão, pode ser NULL
    * @param string $sql String em texto com a consulta a ser excutada
    * @return object mysqli_result
    **/
    public function getQuery($link, $sql)
    {
        if ($result = $link->query($sql)) {
            return $result;
        }
    }

    /**
    * @description Executa uma query sem retorno
    *
    * @param object $link instancia de conexão, pode ser NULL
    * @param string $sql String em texto com a consulta a ser excutada
    * @return bool
    **/
    public function queryIn($link, $sql)
    {
        if ($link->query($sql) === true) {
            return true;
        } else {
            return false;
        }
    }
}