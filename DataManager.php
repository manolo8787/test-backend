<?php

require_once "AppDao.php";

/**
 * @author Guilherme Mendes <guilherme8787@gmail.com>
 * 
 * Essa classe foi escrita em PHP Puro e tem como objetivo organizar o Array retornado em lista-de-compras.php
 * a classe possui metodos para gerar um arquivo csv e gravar no banco
 * 
 */
class DataManager {

    public $lista;

    public $meses = Array(
        "Janeiro", 
        "Fevereiro", 
        "Marco", 
        "Abril", 
        "Maio", 
        "Junho", 
        "Julho", 
        "Agosto", 
        "Setembro", 
        "Outubro", 
        "Novembro", 
        "Dezembro"
    );
    
    public $ordem = Array(
        'Alimentos',
        'Higiene Pessoal',
        'Limpeza'
    );
    
    public function __construct(){
        $this->lista = include_once "lista-de-compras.php";
    }

    /**
    * @description tratarValores ordena o vetor do conteudo da categoria na ordem decrescente e trata as palavras corrigindo os erros de digitação previstos de forma não dinamica
    *
    * @param array $conteudo vetor com o conteudo da categoria
    * @return array
    **/
    public function tratarValores($conteudo){
        $arr = Array(
            'Papel Hignico' =>	'Papel Higiênico',
            'Brocolis' =>	'Brócolis',
            'Chocolate ao leit' =>	'Chocolate ao leite',
            'Sabao em po' =>	'Sabão em pó'
        );
        foreach($conteudo as $key => $val){
            if(isset($arr[$key])){
                $key = $arr[$key];
            }
            $conteudoFinal[$key] = $val;
        }
        if(isset($conteudoFinal)){
            arsort($conteudoFinal);
            return $conteudoFinal;
        } else {
            return $conteudo;
        }
    }
    
    /**
    * @description ordenaCategorias ordena o vetor pelo indice de categoria dentro do indice do mes na ordem solicitada
    *
    * @param array $lista vetor com a lista completa
    * @return array
    **/
    function ordenaCategorias($lista){
        foreach($lista as $mes => $linhaLista){
            foreach($this->ordem as $tipo){
                foreach($linhaLista as $key => $val){
                    $distanciaTipo = levenshtein($tipo, str_replace('_', ' ', $key));
                        if($distanciaTipo < 3){
                            $listaOrdenada[$mes][$tipo] = $this->tratarValores($val);
                        }
                }
            }
        }
        if(isset($listaOrdenada)){
            return $listaOrdenada;
        } else {
            return false;
        }
    }
    
    /**
    * @description ordenaMeses ordena o vetor pelo indice de meses na ordem crescente
    *
    * @return array
    **/
    function ordenaMeses(){
        foreach($this->meses as $mes){
            foreach($this->lista as $key => $val){
                $distancia = levenshtein($mes, $key);
                if($distancia < 2){
                    if($mes == 'Marco'){
                        $mes = 'Março';
                    }
                    $mesOrdenado[$mes] = $val;
                }
            }
        }
        if(isset($mesOrdenado)){
            return $mesOrdenado;
        } else {
            return false;
        }
    }

    /**
    * @description gravarArquivoCsv Gera um arquivo csv a partir do vetor tratado
    *
    * @return void
    **/
    public function gravarArquivoCsv(){
        $lista = $this->ordenaCategorias($this->ordenaMeses());
        $header = 'Mês;Categoria;Produto;Quantidade'.PHP_EOL;
        $linhaParaGravar = '';
        foreach($lista as $mes => $linha){
            $linhaParaGravarMes = $mes.';';
            foreach($linha as $categoria => $produtos){
                $linhaParaGravarCategoria = $categoria.';';
                foreach($produtos as $produto => $quantidade){
                    $linhaParaGravar .= $linhaParaGravarMes.$linhaParaGravarCategoria.$produto.';'.$quantidade.PHP_EOL;
                }
            }
        }
        $arq = fopen('compras-do-ano.csv', 'w');
        fwrite($arq, mb_convert_encoding($header.$linhaParaGravar, 'UTF-8'));
        fclose($arq);
    }

    /**
    * @description Grava no banco de dados os dados do vetor retornado em lista-de-compras.php
    *
    * @return void
    **/
    public function gravaNoBanco(){
        AppDao::possoGravar();
        $arq = file('compras-do-ano.csv');
        if(count($arq) === 1){
            $this->gravarArquivoCsv();
            $arq = file('compras-do-ano.csv');
        }
        $countFinal = 1;
        for($i = 1; $i < count($arq); $i++){
            $linhaExplodida = explode(';', $arq[$i]);
            $idCategoria = AppDao::categoriaIn($linhaExplodida[1]);
            $idProduto = AppDao::produtoIn($linhaExplodida[2], $idCategoria);
            $idItem = AppDao::itemIn($idCategoria, $idProduto, $linhaExplodida[0], $linhaExplodida[3]);
            if($idItem != NULL){
                $countFinal++;
            }
        }
        echo $countFinal." linhas registradas";
    }   

}
