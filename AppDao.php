<?php

require_once "Conn.php";

/**
 * @author Guilherme Mendes <guilherme8787@gmail.com>
 * 
 * Essa classe foi escrita em PHP puro ela é um DAO, manipula o banco de dados.
 * Você deve alterar os parametros de conexão de acordo com 
 * as credenciais do seu banco, é interessante não informar uma database, pois tratamos isso na aplicação
 * caso você use a migrate()
 * 
 */
class AppDao {

    /**
    * @description exbibe saida do console se a transação obteve sucesso
    *
    * @param object $result mysqli_result
    * @param string $quem descrição do que foi executado ex.: Criação de tabel XPTO
    * @return void
    **/
    public static function sucesso($result, $quem){
        if($result != null && $result != false){
            echo 'Sucesso ao '.$quem.PHP_EOL;
        } else {
            echo 'Erro ao '.$quem.PHP_EOL;
        }
    }

    /**
    * @description Cria o banco e as tabelas
    *
    * @return void
    **/
    public static function migrate(){
        $conn = new Conn;
        $link = $conn->link();

        $sql  = "CREATE DATABASE IF NOT EXISTS db_lista_de_compras_guilherme_mendes;    ".PHP_EOL;
        self::sucesso($conn->getQuery($link, $sql), 'criar o banco de dados');

        $sql   = "USE db_lista_de_compras_guilherme_mendes;    ".PHP_EOL;
        self::sucesso($conn->getQuery($link, $sql), 'usar banco criado');

        $sql  = "CREATE TABLE IF NOT EXISTS CATEGORIA ( ".PHP_EOL;
        $sql .= "    ID INT(11) NOT NULL AUTO_INCREMENT,    ".PHP_EOL;
        $sql .= "    NOME VARCHAR(200), ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_CHAVE_PRIMARIA_CATEGORIA PRIMARY KEY (ID),    ".PHP_EOL;
        $sql .= "    CONSTRAINT CATEGORIA_DUPLICADA_UNIQUE UNIQUE KEY (NOME) ".PHP_EOL;
        $sql .= "); ".PHP_EOL;
        self::sucesso($conn->getQuery($link, $sql), 'criar table CATEGORIA');

        $sql  = "CREATE TABLE IF NOT EXISTS PRODUTO (   ".PHP_EOL;
        $sql .= "    ID INT(11) NOT NULL AUTO_INCREMENT,    ".PHP_EOL;
        $sql .= "    NOME VARCHAR(200), ".PHP_EOL;
        $sql .= "    ID_CATEGORIA INT(11) NOT NULL, ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_CHAVE_PRIMARIA_PRODUTO PRIMARY KEY (ID), ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_CATEGORIA_PRODUTO_FOREIGN_CATEGORIA FOREIGN KEY (ID_CATEGORIA) REFERENCES CATEGORIA (ID), ".PHP_EOL;
        $sql .= "    CONSTRAINT PRODUTO_DUPLICADAO_UNIQUE UNIQUE KEY (NOME) ".PHP_EOL;
        $sql .= "); ".PHP_EOL;
        self::sucesso($conn->getQuery($link, $sql), 'criar table PRODUTO');

        $sql  = "CREATE TABLE IF NOT EXISTS ITEM (  ".PHP_EOL;
        $sql .= "    ID INT(11) NOT NULL AUTO_INCREMENT,    ".PHP_EOL;
        $sql .= "    ID_CATEGORIA INT(11) NOT NULL, ".PHP_EOL;
        $sql .= "    ID_PRODUTO INT(11) NOT NULL,   ".PHP_EOL;
        $sql .= "    MES VARCHAR(200) NOT NULL,  ".PHP_EOL;
        $sql .= "    QUANTIDADE INT(11) NOT NULL,  ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_CHAVE_PRIMARIA_ITEM PRIMARY KEY (ID),    ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_CATEGORIA_ITEM_FOREIGN_CATEGORIA FOREIGN KEY (ID_CATEGORIA) REFERENCES CATEGORIA (ID),   ".PHP_EOL;
        $sql .= "    CONSTRAINT ID_PRODUTO_ITEM_FOREIGN_PRODUTO FOREIGN KEY (ID_PRODUTO) REFERENCES PRODUTO (ID)    ".PHP_EOL;
        $sql .= "); ".PHP_EOL;
        self::sucesso($conn->getQuery($link, $sql), 'criar table ITEM');

        $link->close();

        echo 'Migrado!';

    }


    /**
    * @description Insere ou resgata o id da categoria solicitada
    *
    * @param string $categoria Nome da categoria
    * @return int id da categoria
    **/
    public static function categoriaIn($categoria){
        $conn = new Conn;
        $link = $conn->link();
        $sql = "SELECT * FROM db_lista_de_compras_guilherme_mendes.CATEGORIA WHERE UPPER(NOME) = '".trim($categoria)."'; ".PHP_EOL;
        $result = $conn->getQuery($link, $sql);
        if($result->num_rows > 0){
            $idCategoria = $result->fetch_object()->{'ID'};
        } else {
            $sql = "INSERT INTO db_lista_de_compras_guilherme_mendes.CATEGORIA (NOME) VALUES('".trim($categoria)."'); ".PHP_EOL;
            $result = $conn->getQuery($link, $sql);
            @$idCategoria = $link->insert_id;
        }
        $link->close();
        return $idCategoria;
    }

    /**
    * @description Insere ou resgata o id do produto solicitado
    *
    * @param string $produto Nome do produto
    * @param string $idCategoria id da categoria do produto
    * @return int id do produto
    **/
    public static function produtoIn($produto, $idCategoria){
        $conn = new Conn;
        $link = $conn->link();
        $sql = "SELECT * FROM db_lista_de_compras_guilherme_mendes.PRODUTO WHERE UPPER(NOME) = '".trim($produto)."'; ".PHP_EOL;
        $result = $conn->getQuery($link, $sql);
        if($result->num_rows > 0){
            $idProduto = $result->fetch_object()->{'ID'};
        } else {
            $sql = "INSERT INTO db_lista_de_compras_guilherme_mendes.PRODUTO (NOME, ID_CATEGORIA) VALUES('".trim($produto)."', ".$idCategoria."); ".PHP_EOL;
            $result = $conn->getQuery($link, $sql);
            @$idProduto = $link->insert_id;
        }
        $link->close();
        return $idProduto;
    }

    /**
    * @description Insere e retorna o id do item adicionado a lista de compras
    *
    * @param string $produto Nome do produto
    * @param int $idCategoria id da categoria do produto
    * @param int $idProduto id do produto
    * @param string $mes Mes
    * @param int $qntd Quantidade do produto
    * @return int id do item adicionado
    **/
    public static function itemIn($idCategoria, $idProduto, $mes, $qntd){
        $conn = new Conn;
        $link = $conn->link();
        $sql = "INSERT INTO db_lista_de_compras_guilherme_mendes.ITEM (ID_CATEGORIA, ID_PRODUTO, MES, QUANTIDADE) VALUES(".$idCategoria.", ".$idProduto.", '".$mes."', ".preg_replace( "/\r|\n/", "",$qntd)."); ".PHP_EOL;
        $result = $conn->getQuery($link, $sql);
        @$idItem = $link->insert_id;
        $link->close();
        return $idProduto;
    }

    /**
    * @description Verifica se o banco de dados existe e exibe uma mensagem
    *
    * @return void
    **/
    public static function possoGravar(){
        $conn = new Conn;
        $link = $conn->link();
        $sql = "USE db_lista_de_compras_guilherme_mendes;    ".PHP_EOL;
        $result = $conn->getQuery($link, $sql);
        $link->close();
        if(!$result){
            echo 'Erro, o banco não foi criado. Rode o comando "php -r "require \'.\index.php\'; migrarBanco();" ou rode o script SQL "banco.sql" em seu Workbench e tente novamente';
            die;
        }

    }

}