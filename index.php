<?php

require_once "DataManager.php";

function gerarCsv(){
    $manager = new DataManager;
    $manager->gravarArquivoCsv();
}

function gravarNoBanco(){
    $manager = new DataManager;
    $manager->gravaNoBanco();
}

function migrarBanco(){
    AppDao::migrate();
}

function start(){
    echo '
    Bem Vindo
    
    !!!     ABRA O ARQUIVO Conn.php E ALTERE AS VARIAVEIS PARA SE CONECTAR AO SEU BANCO MYSQL 5.5     !!!
    !!!     SE CERTIFIQUE DE TER PERMISSÕES DE GRAVAÇÃO NO DIRETORIO ONDE VOCÊ IRÁ RODAR ESSES SCRIPT     !!!
    
    Voce pode executar os seguintes:
    ------------------------------------------------------------
    |   $ php -r "require \'.\index.php\'; gerarCsv();"         |
    |   $ php -r "require \'.\index.php\'; migrarBanco();"      |
    |   $ php -r "require \'.\index.php\'; gravarNoBanco();"    |
    ------------------------------------------------------------
    
    gerarCsv() -> Vai gerar um arquivo CSV dentro dos conformes do escopo do desafio;
    migrarBanco() -> vai migrar o banco de dados, criar o banco e as tabelas;
    gravarNoBanco() -> vai gravar os itens no banco de dados conforme, tudo tratado bonitinho;
    
    ';
}

function estouComPressa(){
    gerarCsv();
    migrarBanco();
    gravarNoBanco();

    echo "
        ░░░░░░░░░░░░▄▄
        ░░░░░░░░░░░█░░█
        ░░░░░░░░░░░█░░█
        ░░░░░░░░░░█░░░█
        ░░░░░░░░░█░░░░█
        ███████▄▄█░░░░░██████▄
        ▓▓▓▓▓▓█░░░░░░░░░░░░░░█
        ▓▓▓▓▓▓█░░░░░░░░░░░░░░█
        ▓▓▓▓▓▓█░░░░░░░░░░░░░░█
        ▓▓▓▓▓▓█░░░░░░░░░░░░░░█
        ▓▓▓▓▓▓█░░░░░░░░░░░░░░█
        ▓▓▓▓▓▓█████░░░░░░░░░█
        ██████▀░░░░▀▀██████▀
    ";
}